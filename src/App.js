import React from 'react';
import ReactGA from "react-ga4";
import { BrowserRouter as Router,	Route, Switch } from 'react-router-dom';
import './scss/styles.scss';
// import { useGAPageTracker } from './helpers/UseGAEventTracker';
import ScrollToTop from './helpers/ScrollToTop';
import ErrorPage from './components/error.component';
import Header from './components/header.component';
import Footer from './components/footer.component';
import MainPage from './components/mainpage.component';
import config from './config.json';

ReactGA.initialize(config.GAID);

export default function App() {

  // useGAPageTracker(window.location.pathname + window.location.search, "Main page view");
  
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div className="wrapper">
        <header className="s-header">
          <Header />
        </header>
        <ScrollToTop />
        <Switch>
          <Route path="/" exact>
            <MainPage />
          </Route>
          <Route path="*">
            <ErrorPage />
          </Route>
        </Switch>
        <footer className="s-footer">
          <Footer />
        </footer>
      </div>
    </Router>
  );
}
