import React from 'react';

class CardElement extends React.Component {

  render() {
    let items = this.props.items;

    return (
      <>
      { (items.length) ? items.map((item,id) => (
          <div className="col-12 col-md-6 col-lg-4 col-xl-3 col-card" key={id}> 
            <div className="sec-cards__item"> 
              <div className="front"> 
                <div className="img white"> <img src={ this.props.mainURL + item.field_image } alt="" /></div>
                <div className="title"> 
                  <div className="name">{ item.title }</div>
                  <div className="tag">{ item.field_tags }</div>
                </div>
              </div>
              <div className="back">
                <div className="text"> 
                  <p>{ this.props.convertHtmlEntities(item.body) }</p>
                  <p> <a className="external-link" href={ item.field_docs } target="_blank" rel="noreferrer nofollow">Docs
                      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.8333 15.8333H4.16667V4.16667H10V2.5H4.16667C3.24167 2.5 2.5 3.25 2.5 4.16667V15.8333C2.5 16.75 3.24167 17.5 4.16667 17.5H15.8333C16.75 17.5 17.5 16.75 17.5 15.8333V10H15.8333V15.8333ZM11.6667 2.5V4.16667H14.6583L6.46667 12.3583L7.64167 13.5333L15.8333 5.34167V8.33333H17.5V2.5H11.6667Z"></path>
                      </svg></a></p>
                </div>
                <div className="links">
                { item.field_web ? (
                    <>
                      <a className="links__item" href={ item.field_web } title="" target="_blank" rel="noreferrer nofollow"><img src="../pics/socials/web.svg" alt="" /></a>
                    </>
                  ) : null }
                  { item.field_github ? (
                    <>
                      <a className="links__item" href={ item.field_github } title="" target="_blank" rel="noreferrer nofollow"><img src="../pics/socials/github.svg" alt="" /></a>
                    </>
                  ) : null }
                  { item.field_facebook ? (
                    <>
                      <a className="links__item" href={ item.field_facebook } title="" target="_blank" rel="noreferrer nofollow"><img src="../pics/socials/facebook.svg" alt="" /></a>
                    </>
                  ) : null }
                  { item.field_twitter ? (
                    <>
                      <a className="links__item" href={ item.field_twitter } title="" target="_blank" rel="noreferrer nofollow"><img src="../pics/socials/twitter.svg" alt="" /></a>
                    </>
                  ) : null }
                </div>
              </div>
            </div>
          </div>
      )) : (<div className="col-12 col-md-6 col-lg-4 col-xl-3 col-card"><div className="sec-cards__empty">No project yet..<br /><br /></div></div>) }
      </>
    );
  }
}

export default CardElement;