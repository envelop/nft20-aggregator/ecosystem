import React from 'react';

class FilterNetwork extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
     
    return (
      <div className="sec-cards__filter"> 
        <label className="filter-tag">
          <input type="radio" name="category" value="all"  onClick={() => this.props.filterByTag()} onChange={e => {}} checked={(!this.props.curnet)} /><span className="btn btn-gray">All</span>
        </label>
        { this.props.tags.map((tag,id) => (
        <label className="filter-tag" key={id}>
          <input type="radio" name="category" value={tag.toLowerCase()} onClick={() => this.props.filterByTag(tag.toLowerCase())} onChange={e => {}} checked={(this.props.curnet === tag.toLowerCase())} /><span className="btn btn-gray">{tag}</span>
        </label>              
        ))}
      </div>
    );
  }
}    

export default FilterNetwork;