import { Link } from 'react-router-dom';

import Logo from '../pics/logo.svg';
import Youtube from '../pics/socials/youtube.svg';
import LinkedIn from '../pics/socials/linkedin.svg';
import Discord from '../pics/socials/discord.svg';
import Telegram from '../pics/socials/telegram.svg';
import TelegramDeweb from '../pics/socials/nft-news.svg';
import Twitter from '../pics/socials/twitter.svg';
import Medium from '../pics/socials/medium.svg';

const Header = () => (
  <div className="container">
    <Link className="header__logo scroll-link" to="/" rel="canonical"><img src={ Logo } alt="ENVELOP" /></Link>
    <div><a className="btn btn-header d-lg-none" href="https://app.envelop.is" target="_blank" rel="noreferrer">App</a><a className="btn btn-header d-lg-none" href="https://envelop.is/dapps" target="_blank" rel="noreferrer">Our dApps</a></div>
    <div className="header__nav"><a className="btn btn-header d-none d-lg-flex order-lg-2" href="https://app.envelop.is" target="_blank" rel="noreferrer">App</a><a className="btn btn-header d-none d-lg-flex order-lg-2" href="https://envelop.is/dapps" target="_blank" rel="noreferrer">Our dApps</a>
      <div className="order-lg-3">
        <ul className="socials">
          <li><a href="https://t.me/envelop_en" target="_blank" rel="noreferrer" title="Telegram"><img src={ Telegram } alt="ENVELOP telegram group" /></a></li>
          <li><a href="https://www.youtube.com/c/ENVELOP" target="_blank" rel="noreferrer" title="YouTube"><img src={ Youtube } alt="ENVELOP. NFTs YouTube Channel" /></a></li>
          <li><a href="https://www.linkedin.com/company/niftsy" target="_blank" rel="noreferrer" title="LinkedIn"><img src={ LinkedIn } alt="NIFTSY is token" /></a></li>
          <li><a href="https://discord.gg/gtYcjqq76f" target="_blank" rel="noreferrer" title="Discord"><img src={ Discord } alt="ENVELOP Discord group" /></a></li>
          <li><a href="https://envelop.medium.com/" target="_blank" rel="noreferrer" title="Medium"><img src={ Medium } alt="Blog about Web 3.0" /></a></li>
          <li><a href="https://twitter.com/Envelop_project" target="_blank" rel="noreferrer" title="Twitter"><img src={ Twitter } alt="Our twitter" /></a></li>
          <li><a href="https://t.me/nonfungible_web" target="_blank" rel="noreferrer" title="NFT 2.0 News"><img src={ TelegramDeweb } alt="NFT 2.0 News" /></a></li>
        </ul>
      </div>
    </div>
  </div>
);

export default Header;