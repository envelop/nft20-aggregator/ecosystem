import React from 'react';
import CardElement from './card.component';
import FilterNetwork from './filternets.component';
import config from '../config.json';

class Cards extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        json: [],
        items: [],
        stat: [],
        tags: [],
        curnet: '',
        searchText: '',
        param: '',
        loadnets: 7,
        curpage: 0,
        curtotal: 0,
        alltotal: 0,
        loaditems: 0,
        curitem: {},
        isLoaded: false
      };
      this.timeout = 0;
    }

    async componentDidMount() {
      let url = config.mainURL + config.dataURL + config.formatURL;
      try {
        fetch(url)
          .then((res) => res.json())
          .then((json) => {
        		if ('error' in json) {
        			throw new Error( json["error"] );
        		}
        		else {
              const tags = this.getTags(json);
              this.setState({
                json: json,
                items: json,
                tags: tags,
                curpage: 0,
                curtotal: json.length,
                loaditems: json.length,
                isLoaded: true
              });
            }
          })
          .catch(e => {
            console.log(e);
          });
      } catch(e) {
        console.log(e);
      }
    }

    getTags(obj) {
      const allTags = Array.from(new Set(obj.map((item) => item.field_tags)));
      let filteredTags = allTags.filter((tag) => {return tag !== "Others"});
      filteredTags.push("Others");
      return filteredTags;
    }

    filterByTag = (tag) => {
      let filtered;
      if(tag) {
        filtered = this.state.json.filter(item => {return item.field_tags.toLowerCase() === tag})
      }
      else {
        filtered = this.state.json;
      }
      this.setState({
        curnet: tag,
        items: filtered,
        curtotal: filtered.length
      });
    }
    
    convertHtmlEntities(input) {
      const entities = {
        '&#039;': "'",
        '&quot;': '"',
        '&amp;': '&',
        '&nbsp;': ' ',
      };
      return input.replace(/&#?\w+;/g, match => entities[match]);
    }

    render() {
        const { isLoaded, items, tags, loaditems, loadnets, curnet, curtotal } = this.state;

        return (
          <section className="sec-cards"> 
            <div className="container"> 
              <div className="sec-cards__filter">
                <FilterNetwork filterByTag={this.filterByTag} getNetworkFromUrl={this.getNetworkFromUrl} curnet={curnet} loadnets={loadnets} items={items} tags={tags} />
                <div className="cards-total">
                  { curtotal ? (
                        <strong>Total: <span>{curtotal} NFT project{Number(curtotal) !== 1 ? 's' : ''}</span></strong>
                        ) : null }
                </div>
              </div>
              <div className="row">
                {
                  !isLoaded ? (
                    <p className="loading">Loading..</p>
                  ) : (
                    <CardElement items={items} mainURL={config.mainURL} convertHtmlEntities={this.convertHtmlEntities} />
                  )
                }
              </div>
              <div className="row">
                <div className="col-12">
                {
                  isLoaded && loaditems === 8 && items.length ? (
                    <div className="btn btn-gray" onClick={ () => { this.loadMore() } }>Load more</div>                   
                  ) : ( ''
                  )
                }
                </div>
              </div>
            </div>
          </section>
        );
    }
}    

export default Cards;