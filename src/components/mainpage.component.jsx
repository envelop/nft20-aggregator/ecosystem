import { useLocation } from 'react-router';
import { Helmet } from 'react-helmet';
import Intro from './intro.component';
import Cards from './cards.component';
import Form from './form.component';
import smoothscroll from 'smoothscroll-polyfill';

smoothscroll.polyfill();

const MainPage = (props) => {
  const location = useLocation();
  return (
    <>
      <Helmet>
        <title>DAO ENVELOP. Ecosystem</title>
        <meta name="description" content="Adjustable decentralized financial assets. Programmable NFTs." />
      </Helmet>
      <main className="s-main">
        <Intro />
        <Cards location={location} {...props} />
        <div className="divider left"></div>
        <Form />
      </main>
    </>
  );
}

export default MainPage;