import { Helmet } from 'react-helmet';

const ErrorPage = () => (
  <>
    <Helmet>
        <title>Error - NFT 2.0 by Envelop</title>
        <meta name="description" content="" />
    </Helmet>
    <section className="sec-microdaointro" id="microdaointro">
      <div className="container">
        <div className="intro__text">
          <h1>Error</h1>
          <p className="h1_sub">PAGE NOT FOUND</p>
          <p>We couldn't find the page you're looking for.</p>
        </div>
      </div>
    </section>
  </>
);

export default ErrorPage;