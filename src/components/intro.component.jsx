// import { useGAEventTracker } from '../helpers/UseGAEventTracker';
import Bgimgright from '../pics/bg/bg-lg-right-6.svg';
import Bgimgleft from '../pics/bg/bg-lg-left-1.svg';

const Intro = () => {
  
  // const gaEventTracker = useGAEventTracker('Intro');
  
  return(
    <section className="sec-intro">
      <img src={ Bgimgleft } className="sec-bg bg-left d-none d-xl-block" alt="" />
      <img src={ Bgimgright } className="sec-bg bg-right d-none d-xl-block" alt="" />
      <div className="container">
        <div className="row">
          <div className="col-lg-5"> 
            <h1>ENVELOP <span className="text-grad">ECOSYSTEM</span></h1>
          </div>
          <div className="col-lg-6">
            <p>DAO ENVELOP is a cross-chain multitool for working with wrap NFTs, which in turn allow creating a variety of solutions based on the Protocol, Oracle and Index, such as ido-launchpad, farming/staking, NFT-tickets, SBT and more. </p>
            <p>The ENVELOP ecosystem is all projects where you can find:<br className="d-none d-xl-block" /> NIFTSY token, wrap NFT created with the Protocol, and other ENVELOP-based solutions.</p>
            <p>Also, please note that this ecosystem is updated approximately once a week, which means that not only can you find new projects, but you can also add your own. To do so, send a request with the hashtag <span className="green-text">#ecosystem</span> to our <a href="https://t.me/envelop_en" target="_blank" rel="noreferrer">telegram</a> group and we will be sure to give you a bounty.</p>
          </div>
        </div>
      </div>
    </section>
  )
};

export default Intro;