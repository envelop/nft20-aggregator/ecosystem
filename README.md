# Ecosystem Web App
## Docker Dev Environment with local NGINX
```bash
docker run --rm  -it -v $PWD:/app node:16 /bin/bash -c 'cd /app && npm install --force && chmod -R 777 node_modules'
```
```bash
docker run --rm  -it  -v $PWD:/app node:16 /bin/bash -c 'cd /app && npm run build'
```

```bash

docker compose  -f docker-compose-local.yaml up
```

